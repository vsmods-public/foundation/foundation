namespace Foundation.SynchronizedStorage
{
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;
    using Vintagestory.API.Util;

    /// <summary>
    /// A helper class to reduce the boilerplate code of synchronizing data from server to client (one way)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SynchronizeOneWayStorage<T> where T : class, new()
    {
        public string Name { get; }
        public ICoreAPI Api { get; }
        private IServerNetworkChannel serverChannel;
        private IClientNetworkChannel clientChannel;
        private T storage;

        public T Storage
        {
            get
            {
                return this.storage;
            }

            set
            {
                this.storage = value;
                this.Refresh();
            }
        }

        public event System.Action<T> OnChanged;

        public void Refresh()
        {
            this.SendDataToClients();
            OnChanged?.Invoke(this.Storage);
        }

        public SynchronizeOneWayStorage(string name, ICoreAPI api)
        {
            this.Name = name;
            this.Api = api;
        }

        #region Server
        public virtual void StartServerSide(ICoreServerAPI api)
        {
            api.Event.SaveGameLoaded += () => this.OnSaveGameLoading(api);
            api.Event.GameWorldSave += () => this.OnSaveGameSaving(api);
            api.Event.PlayerJoin += player => this.SendDataToClients();

            this.serverChannel = api.Network.RegisterChannel(this.Name)
                .RegisterMessageType<SynchronizedStorageMessage<T>>();
        }
        protected virtual void OnSaveGameSaving(ICoreServerAPI api)
        {
            api.WorldManager.SaveGame.StoreData(this.Name, SerializerUtil.Serialize(this.Storage));
        }

        protected virtual void OnSaveGameLoading(ICoreServerAPI api)
        {
            var data = api.WorldManager.SaveGame.GetData(this.Name);
            this.Storage = data != null ? SerializerUtil.Deserialize<T>(data) : new T();
        }

        protected virtual void SendDataToClients()
        {
            if (this.Api is ICoreServerAPI)
            {
                this.serverChannel?.BroadcastPacket(new SynchronizedStorageMessage<T>() { Name = Name, Storage = Storage });
            }
        }
        #endregion

        #region Client

        public virtual void StartClientSide(ICoreClientAPI api)
        {
            this.clientChannel = api.Network.RegisterChannel(this.Name)
                .RegisterMessageType<SynchronizedStorageMessage<T>>()
                .SetMessageHandler<SynchronizedStorageMessage<T>>(this.OnClientDataReceived);
        }

        protected virtual void OnClientDataReceived(SynchronizedStorageMessage<T> msg)
        {
            if (msg.Name == this.Name)
            {
                this.Storage = msg.Storage;
            }
        }
        #endregion
    }
}
