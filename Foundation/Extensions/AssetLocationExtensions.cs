namespace Foundation.Extensions
{
    using Vintagestory.API.Common;

    public static class AssetLocationExtensions
    {
        public static AssetLocation CopyWithPathReplaced(this AssetLocation assetLocation, int posFromLeft, string newPathPart)
        {
            var oldPathPart = assetLocation.FirstPathPart(posFromLeft);
            var newPath = assetLocation.Path.Replace(oldPathPart, newPathPart);

            return assetLocation.CopyWithPath(newPath);
        }
    }
}
