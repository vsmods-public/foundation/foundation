namespace Foundation.Extensions
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Dynamic;

    public static class ObjectExtensions
    {
        public static List<T> AsList<T>(this T obj)
        {
            return new List<T> { obj };
        }

        public static dynamic ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
            {
                expando.Add(property.Name, property.GetValue(value));
            }

            foreach (var field in value.GetType().GetFields())
            {
                expando.Add(field.Name, field.GetValue(value));
            }

            return expando as ExpandoObject;
        }
    }
}
