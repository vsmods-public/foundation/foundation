namespace Foundation.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Vintagestory.API.Server;
    using Vintagestory.API.Util;
    using Vintagestory.GameContent;

    /// <summary>
    /// This class is a workaround because the api has no easy way to add waypoints
    /// </summary>
    public static class WorldMapManagerExtensions
    {
        public static WaypointMapLayer GetWaypointMapLayer(this WorldMapManager mapManager)
        {
            return mapManager.MapLayers.OfType<WaypointMapLayer>().FirstOrDefault();
        }

        public static void AddWaypointToPlayer(this WorldMapManager mapManager, Waypoint waypoint, IServerPlayer player)
        {
            var mapLayer = mapManager.GetWaypointMapLayer();
            if (mapLayer != null)
            {
                mapLayer.Waypoints.Add(waypoint);
                mapManager.ResendWaypoints(player, mapLayer);
            }
        }

        public static void ResendWaypoints(this WorldMapManager mapManager, IServerPlayer player, WaypointMapLayer mapLayer)
        {
            //copied from WaypointMapLayer because the method is not public
            var memberOfGroups = player.ServerData.PlayerGroupMemberships;
            var hisMarkers = new List<Waypoint>();

            foreach (var marker in mapLayer.Waypoints)
            {
                if (player.PlayerUID != marker.OwningPlayerUid && !memberOfGroups.ContainsKey(marker.OwningPlayerGroupId))
                {
                    continue;
                }

                hisMarkers.Add(marker);
            }
            mapManager.SendMapDataToClient(mapLayer, player, SerializerUtil.Serialize(hisMarkers));
        }
    }
}
