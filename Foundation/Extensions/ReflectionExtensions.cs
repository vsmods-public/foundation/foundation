namespace Foundation.Extensions
{
    using HarmonyLib;

    public static class ReflectionExtensions
    {
        public static T GetField<T>(this object instance, string fieldname)
        {
            return (T)AccessTools.Field(instance.GetType(), fieldname).GetValue(instance);
        }
    }
}
