namespace Foundation.Extensions
{
    using Vintagestory.API.Common;

    public static class LoggerExtensions
    {
        public static void LogRaw(this ILogger logger, EnumLogType logType, string message)
        {
            logger.Log(logType, "{0}", message);
        }
    }
}
