namespace Foundation.StaticStorage
{
    using System.Collections.Concurrent;

    public static class StaticStorage
    {
        private static readonly ConcurrentDictionary<string, object> KeyToObjectDict = new ConcurrentDictionary<string, object>();

        public static void SetValue(string key, object value)
        {
            KeyToObjectDict[key] = value;
        }

        public static T GetValue<T>(string key, T @default = default)
        {
            if (!KeyToObjectDict.TryGetValue(key, out var value))
            {
                return @default;
            }

            return (T)value;
        }

        public static void Add(string key, int modifier = 1)
        {
            KeyToObjectDict.AddOrUpdate(key, modifier, (dictKey, dictValue) => dictValue = (int)dictValue + modifier);
        }

        public static void Subtract(string key, int modifier = 1)
        {
            KeyToObjectDict.AddOrUpdate(key, modifier * -1, (dictKey, dictValue) => dictValue = (int)dictValue - modifier);
        }
    }
}
