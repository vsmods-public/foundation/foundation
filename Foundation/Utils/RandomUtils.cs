namespace Foundation.Utils
{
    using System;

    public static class RandomUtils
    {
        [ThreadStatic]
        private static Random __random;

        public static Random Random => __random ?? (__random = new Random((int)((1 + System.Threading.Thread.CurrentThread.ManagedThreadId) * DateTime.UtcNow.Ticks)));
    }
}
