namespace Foundation.Utils
{
    using System.IO;

    public static class FileUtils
    {
        public static void CreateFolderIfNotExists(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
        }
    }
}
