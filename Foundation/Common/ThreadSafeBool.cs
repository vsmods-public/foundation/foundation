namespace Foundation.Common
{
    using System.Threading;

    public struct ThreadSafeBool
    {
        // default is false, set 1 for true.
        private int threadSafeBoolBackValue;

        public bool Value
        {
            get
            {
                return Interlocked.CompareExchange(ref this.threadSafeBoolBackValue, 1, 1) == 1;
            }
            set
            {
                if (value)
                {
                    Interlocked.CompareExchange(ref this.threadSafeBoolBackValue, 1, 0);
                }
                else
                {
                    Interlocked.CompareExchange(ref this.threadSafeBoolBackValue, 0, 1);
                }
            }
        }

        public ThreadSafeBool(bool value = false)
        {
            this.threadSafeBoolBackValue = value ? 1 : 0;
        }

        public static implicit operator bool(ThreadSafeBool threadSafeBool)
        {
            return threadSafeBool.Value;
        }

        public static implicit operator ThreadSafeBool(bool b)
        {
            return new ThreadSafeBool(b);
        }
    }
}
