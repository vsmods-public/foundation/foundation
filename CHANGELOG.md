## 0.4.15 (2021-08-24)

### New feature (1 change)

- [Add StringExtensions.Replace](vsmods-public/foundation/foundation@de1efa3941c6d53d7f0fec698cad23089fa31670)

## 0.4.14 (2021-08-19)

### Bug fix (1 change)

- [Add proper Gitlab URL for SourceLink support](vsmods-public/foundation/foundation@2e4a8edca903674572c613c75e5a1503f86b168f)

## 0.4.13 (2021-06-28)

### New feature (2 changes)

- [Added ThreadSafeBool](vsmods-public/foundation/foundation@a886116e070256a84cb9e95f6df4bb091c83808b)
- [Added ModConfig extensions which make it easy to handle configuration files for mods](vsmods-public/foundation/foundation@83146d3a3cbb1dedd415cab0bb405850f7e7c272)

## 0.4.5 (2021-06-19)

### Bug fix (1 change)

- [Fix zip directory output so no temp folder is expected](vsmods-public/foundation/foundation@bd202dc7074c36046474f9d9155043e53eea98d4)

## 0.4.2 (2021-06-19)

### New feature (1 change)

- [Use custom built ILRepack package to support dotnet build](vsmods-public/foundation/foundation@bcf823d3affeb8fe30775c0a171af2ba73ac9960)
